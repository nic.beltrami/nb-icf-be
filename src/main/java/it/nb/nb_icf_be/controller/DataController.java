package it.nb.nb_icf_be.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.nb.nb_icf_be.dto.DataDTO;
import it.nb.nb_icf_be.mapper.DataMapper;
import it.nb.nb_icf_be.model.Data;
import it.nb.nb_icf_be.service.DataService;

@RestController
@CrossOrigin
@RequestMapping("/data")
public class DataController {

	@Autowired
	private DataService dataService;

	@Autowired
	private DataMapper dataMapper;

	@GetMapping
	public List<DataDTO> findAll() {
		return dataMapper.toDtos(dataService.findAll());
	}

	@PostMapping(value = "/{id}/decrypt")
	public DataDTO getDecrypted(@RequestBody DataDTO dataDTO) {
		Data data = dataMapper.fromDto(dataDTO);
		return dataMapper.toDto(dataService.getDecryptedData(data));
	}

	@PostMapping
	public void insert(@RequestBody DataDTO dataDTO) {
		dataService.insert(dataMapper.fromDto(dataDTO));
	}

	@PutMapping
	public void update(@RequestBody DataDTO dataDTO) {
		dataService.update(dataMapper.fromDto(dataDTO));
	}

	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		dataService.deleteById(id);
	}

}
