package it.nb.nb_icf_be.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import it.nb.nb_icf_be.model.Data;

@Mapper
public interface MyBatisDataRepository {

	@Select("SELECT ID, SECRETVALUE AS VALUE FROM SECRETVALUES")
	public List<Data> findAll();

	@Insert("INSERT INTO SECRETVALUES(ID, SECRETVALUE) VALUES (#{id}, #{value})")
	public int insert(Data SECRETVALUES);

	@Update("UPDATE SECRETVALUES SET SECRETVALUE=#{value} WHERE id=#{id}")
	public int update(Data SECRETVALUES);

	@Delete("DELETE FROM SECRETVALUES WHERE id = #{id}")
	public int deleteById(long id);

}
