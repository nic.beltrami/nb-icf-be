package it.nb.nb_icf_be.service;

import it.nb.nb_icf_be.model.Data;

public interface EncryptionService {

	public String encrypt(Data data);

	public String decrypt(Data data);

}
