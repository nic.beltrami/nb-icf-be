package it.nb.nb_icf_be.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.nb.nb_icf_be.model.Data;
import it.nb.nb_icf_be.repository.MyBatisDataRepository;

@Service
public class DataServiceImpl implements DataService {

	@Autowired
	private MyBatisDataRepository dataRepository;

	@Autowired
	private EncryptionService encryptionService;

	@Override
	public List<Data> findAll() {
		return dataRepository.findAll();
	}

	@Override
	public Data getDecryptedData(Data data) {
		if (data.getValue().isEmpty()) {
			throw new IllegalArgumentException();
		}
		data.setValue(this.encryptionService.decrypt(data));
		return data;
	}

	@Override
	public void insert(Data data) {
		data.setValue(this.encryptionService.encrypt(data));
		dataRepository.insert(data);
	}

	@Override
	public void update(Data data) {
		data.setValue(this.encryptionService.encrypt(data));
		dataRepository.update(data);
	}

	@Override
	public void deleteById(Long id) {
		if (id == null) {
			throw new IllegalArgumentException();
		}
		dataRepository.deleteById(id);

	}

}
