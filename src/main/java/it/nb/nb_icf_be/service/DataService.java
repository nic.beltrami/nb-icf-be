package it.nb.nb_icf_be.service;

import java.util.List;

import it.nb.nb_icf_be.model.Data;

public interface DataService {

	List<Data> findAll();

	Data getDecryptedData(Data data);

	void insert(Data data);

	void update(Data data);

	void deleteById(Long id);

}
