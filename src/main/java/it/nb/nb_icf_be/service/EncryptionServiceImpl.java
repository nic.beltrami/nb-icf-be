package it.nb.nb_icf_be.service;

import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.stereotype.Service;

import it.nb.nb_icf_be.model.Data;

@Service
public class EncryptionServiceImpl implements EncryptionService {

	private static final String PASSWORD = "cksbowidvbcowib87r982w698ycxhajcbk--csaigfqi78yyiyo";

	@Override
	public String encrypt(Data data) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPasswordCharArray(PASSWORD.toCharArray());
		return textEncryptor.encrypt(data.getValue());
	}

	@Override
	public String decrypt(Data data) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPasswordCharArray(PASSWORD.toCharArray());
		return textEncryptor.decrypt(data.getValue());
	}

}
