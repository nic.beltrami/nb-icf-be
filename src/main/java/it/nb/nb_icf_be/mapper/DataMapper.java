package it.nb.nb_icf_be.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import it.nb.nb_icf_be.dto.DataDTO;
import it.nb.nb_icf_be.model.Data;

@Mapper
public interface DataMapper {

	DataDTO toDto(Data data);

	Data fromDto(DataDTO dataDTO);

	List<DataDTO> toDtos(List<Data> datas);

	List<Data> fromDtos(List<DataDTO> dataDTOs);

}
