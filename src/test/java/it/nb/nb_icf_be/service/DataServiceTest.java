package it.nb.nb_icf_be.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import it.nb.nb_icf_be.model.Data;
import it.nb.nb_icf_be.repository.MyBatisDataRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class DataServiceTest {

	@InjectMocks
	private DataServiceImpl dataService;

	@Mock
	private MyBatisDataRepository dataRepository;

	@Mock
	private EncryptionServiceImpl encryptionService;

	private Data data;

	@Before
	public void setup() {
		data = new Data();
		data.setValue("J983uBBW6GaQcay5zvEcNQ==");
	}

	/**
	 * Check if the findAll on service call findAll on repository
	 */
	@Test()
	public void findAll() {
		Mockito.doReturn(Collections.emptyList()).when(dataRepository).findAll();
		this.dataService.findAll();
		verify(dataRepository, times(1)).findAll();
	}

	/**
	 * Check if the insert on service call insert on repository
	 */
	@Test()
	public void insert() {
		this.dataService.insert(data);
		verify(dataRepository, times(1)).insert(data);
	}

	/**
	 * Check if the update on service call update on repository
	 */
	@Test()
	public void update() {
		this.dataService.update(data);
		verify(dataRepository, times(1)).update(data);
	}

	/**
	 * Check if the descryptedData on service call descryptedData on service for
	 * decryption
	 */
	@Test()
	public void getDecryptedData() {
		this.dataService.getDecryptedData(data);
		verify(encryptionService, times(1)).decrypt(data);
	}

	/**
	 * Cannot decrypt empty value
	 */
	@Test(expected = IllegalArgumentException.class)
	public void cannotDecryptedDataWithEmptyValue() {
		data.setValue("");
		this.dataService.getDecryptedData(data);
		verify(encryptionService, times(1)).decrypt(data);
	}

	/**
	 * Check if the delete on service call delete on repository
	 */
	@Test()
	public void deleteById() {
		this.dataService.deleteById(1L);
		verify(dataRepository, times(1)).deleteById(1L);

	}

	/**
	 * Cannot delete if id is null
	 */
	@Test(expected = IllegalArgumentException.class)
	public void deleteWithIdNull() {
		this.dataService.deleteById(null);
	}

}
