package it.nb.nb_icf_be.service;

import static org.junit.Assert.assertTrue;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import it.nb.nb_icf_be.model.Data;

@RunWith(SpringJUnit4ClassRunner.class)
public class EncryptionServiceTest {

	@InjectMocks
	private EncryptionServiceImpl encryptionService;

	private Data data;

	@Before
	public void setup() {
		data = new Data();
		data.setValue("J983uBBW6GaQcay5zvEcNQ==");
	}

	/**
	 * Check if encryption return a string with char
	 */
	@Test()
	public void getEncryptionData() {
		String dataEncrypted = this.encryptionService.encrypt(data);
		assertTrue(dataEncrypted.length() > 0);
	}

	/**
	 * Check if descryption is correct
	 */
	@Test()
	public void getDecryptedData() {
		String dataDescrypted = this.encryptionService.decrypt(data);
		assertTrue("gatto".equals(dataDescrypted));
	}

	/**
	 * Check the impossible descryption
	 */
	@Test(expected = EncryptionOperationNotPossibleException.class)
	public void invalidDecryptedData() {
		data.setValue("not_valid");
		String dataDescrypted = this.encryptionService.decrypt(data);
		assertTrue("gatto".equals(dataDescrypted));
	}
}
