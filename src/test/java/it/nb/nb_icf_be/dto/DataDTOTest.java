package it.nb.nb_icf_be.dto;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class DataDTOTest {

	private DataDTO dataDTO;

	private static final String JSON = "{ " + "id: 'Long'," + "value: 'String'}";

	@Before
	public void setup() {
		dataDTO = new DataDTO();
	}

	/**
	 * Check fields of DTO respect JSON
	 * 
	 * @throws JSONException
	 */
	@Test
	public void dataDTOTest() throws JSONException {
		assertTrue(this.checkDTO());
	}

	private boolean checkDTO() throws JSONException {

		boolean res = true;

		JSONObject jsonObj = new JSONObject(JSON);

		List<Field> fields = Arrays.asList(dataDTO.getClass().getDeclaredFields());

		if (fields.size() != jsonObj.length()) {
			res = false;
		} else {
			for (Field f : fields) {
				Object type = jsonObj.get(f.getName());
				if (type == null || !f.getType().getSimpleName().equals(type)) {
					res = false;
				}
			}
		}

		return res;
	}
}
