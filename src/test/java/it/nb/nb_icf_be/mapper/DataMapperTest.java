package it.nb.nb_icf_be.mapper;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import it.nb.nb_icf_be.dto.DataDTO;
import it.nb.nb_icf_be.model.Data;

@RunWith(SpringJUnit4ClassRunner.class)
public class DataMapperTest {

	private DataMapper mapper;

	@Before
	public void setup() {
		mapper = Mappers.getMapper(DataMapper.class);
	}

	@Test()
	public void testMapDtoToEntity() {

		DataDTO dataDTO = new DataDTO();
		dataDTO.id = 1L;
		dataDTO.value = "test";

		Data data = mapper.fromDto(dataDTO);

		assertTrue(data.getId().equals(1L));
		assertTrue(data.getValue().equals("test"));
	}

	@Test()
	public void testEntityToDto() {

		Data data = new Data();
		data.setId(1L);
		data.setValue("test");

		DataDTO dataDTO = mapper.toDto(data);

		assertTrue(dataDTO.id.equals(1L));
		assertTrue(dataDTO.value.equals("test"));
	}
}
